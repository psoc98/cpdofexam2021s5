package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class TestMinMax {
    @Test
    public void testAdd() throws Exception {

        int k= new MinMax().f(6,5);
        assertEquals(6, k);
        
    }

    @Test   
    public void testBar() throws Exception {

        String k= new MinMax().bar("something");
        assertEquals("something", k);

    }
}

